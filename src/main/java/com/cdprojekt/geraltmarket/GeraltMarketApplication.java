package com.cdprojekt.geraltmarket;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories
public class GeraltMarketApplication {

  public static void main(String[] args) {
    SpringApplication.run(GeraltMarketApplication.class, args);
  }
}
