package com.cdprojekt.geraltmarket.admin;

import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.core.oidc.user.OidcUser;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AdminController {

  @GetMapping("/")
  public String login(final Model model, @AuthenticationPrincipal final OidcUser principal) {
    return "index";
  }
}
