package com.cdprojekt.geraltmarket.game;

public enum GameStatus {
  DRAFT,
  PUBLISHED
}
