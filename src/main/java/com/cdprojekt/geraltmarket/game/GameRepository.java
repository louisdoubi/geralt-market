package com.cdprojekt.geraltmarket.game;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

public interface GameRepository extends PagingAndSortingRepository<Game, Long> {
  @Query("SELECT g FROM Game g WHERE (:name is null or g.name like %:name%)")
  Page<Game> findAllByNameContaining(@Param("name") final String name, final Pageable pageable);
}
