package com.cdprojekt.geraltmarket.game;

import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDate;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

@Entity
@Table(schema = "market", name = "game")
@Data
public class Game {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column
  private Long id;

  @Column(nullable = false)
  private String name;

  @Column(nullable = false)
  private String description;

  @Column(nullable = false)
  private LocalDate releaseDate;

  @Column(nullable = false, name = "price")
  private BigDecimal priceCents;

  @Column(nullable = false)
  @CreationTimestamp
  private Instant createdAt;

  @Column(nullable = false)
  private String createdBy;

  @Column(nullable = false)
  @UpdateTimestamp
  private Instant updatedAt;

  @Column(nullable = false)
  private String updatedBy;

  @Column(nullable = false)
  @Enumerated(EnumType.STRING)
  private GameStatus status;
}
