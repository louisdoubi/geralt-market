package com.cdprojekt.geraltmarket.game;

import javax.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Transactional
public class GameService {
  private final GameRepository gameRepository;

  public Page<Game> getAllGame(final String name, final int page, final int size) {
    final Pageable pageable = PageRequest.of(page, size);
    return gameRepository.findAllByNameContaining(name, pageable);
  }

  public Game updateGame(final Long id, final GameDto gameDto) {
    final String username = SecurityContextHolder.getContext().getAuthentication().getName();
    final Game game = getGame(id);

    game.setPriceCents(gameDto.priceCents());
    game.setUpdatedBy(username);
    game.setName(gameDto.name());
    game.setStatus(gameDto.status());
    return gameRepository.save(game);
  }

  private Game getGame(final Long id) {
    return gameRepository.findById(id).orElseThrow(RuntimeException::new);
  }
}
