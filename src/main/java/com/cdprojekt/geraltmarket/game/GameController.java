package com.cdprojekt.geraltmarket.game;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api/v1")
public class GameController {
  private final GameService gameService;

  @GetMapping("/games")
  public ResponseEntity<Page<Game>> searchGames(
      @RequestParam(required = false) final String name,
      @RequestParam(defaultValue = "0") final int page,
      @RequestParam(defaultValue = "100") final int size) {
    return ResponseEntity.ok(gameService.getAllGame(name, page, size));
  }

  @PutMapping("/games/{id}")
  public ResponseEntity<Game> updateGame(
      @PathVariable final Long id, @RequestBody final GameDto gameDto) {
    return ResponseEntity.ok(gameService.updateGame(id, gameDto));
  }
}
