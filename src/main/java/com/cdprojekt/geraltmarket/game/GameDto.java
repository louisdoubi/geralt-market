package com.cdprojekt.geraltmarket.game;

import java.math.BigDecimal;
import java.time.LocalDate;

public record GameDto(
    Long id, String name, LocalDate releaseDate, BigDecimal priceCents, GameStatus status) {}
