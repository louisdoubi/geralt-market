package com.cdprojekt.geraltmarket.customer;

public enum CustomerStatus {
  ACTIVATED,
  DISABLED
}
